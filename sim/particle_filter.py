try:
  from tkinter import Tk, Canvas, Toplevel
except ImportError:
  from Tkinter import Tk, Canvas, Toplevel
from sim.viewer.Object import MOVABLE_COLORS, IMMOVABLE_COLORS, TEXT_COLOR, BLACK, BACKGROUND, TABLE_BACKGROUND, TABLE_COLOR, ARM_NOT_EMPTY_COLOR

import copy
# from termcolor import colored
import numpy as np
import sys
# import pandas as pd 
import math
from sim.util.utils import user_input, ERROR, WARN, GOOD, INFO
# from sim.ilp import Predicate, Atom, Literal, Term, PreEff, Rule

def UniformSampler( inputTuple ):
  (n, left, right) = inputTuple
  return tuple(np.random.uniform( left, right, n ))

def GaussianSampler( inputTuple ):
  (n, mean, sd) = inputTuple
  return tuple(np.random.normal( mean, sd, n ))

# predicate initialization:
# predicateSet = ( ( "atPos", 1, "2-dim" ), ( "physicsOk", 0 ) )

# initial conditions:
# initPreds = ( 
#   ("atPos", 0.6, ( "prior", "2-dim", UniformSampler, (2, -0.5, 0.5) ) ),
# )
initPreds = ( 
  ("atPos", 0.6, ( "prior", "2-dim", GaussianSampler, (2, 0, 0.5) ) ),
)


class ParticleFilter:
  def __init__(self, predList, initialPredicates, numParticles = 20, stayThreshold = 0.5):
    self.predicates = dict() # dictionary of available predicates
    self.initPredicates(predList)
    self.numParticles = numParticles
    self.particles = []
    self.initParticles(initialPredicates)
    self.stayThreshold = stayThreshold

  def initPredicates( self, predicates ):
    for predicate in predicates:
      self.predicates[predicate.name] = predicate

    # initialize the dictionary of known predicates
    # for predSpec in tuples:
    #   predName = predSpec[0]
    #   predArity = predSpec[1]
    #   predTypes = []
    #   if predArity > 0:
    #     predTypes = list( predSpec[2:] )
    #   self.predicates[predName] = Predicate(predName, predArity, predTypes)

  def initParticles(self, initCond):
    # initialize n particles
    for i in range(self.numParticles):
      ple = []
      for atomSpec in initCond:
        predName = atomSpec[0]
        terms = []
        for trm in atomSpec[2:]:
          if trm[0] == "prior":
            terms += [Term( "object", trm[1], trm[2](trm[3]) )]
          elif trm[0] == "value":
            terms += [Term( "object", trm[1], trm[2] )]

        atom = Atom( self.predicates[predName], terms, atomSpec[1] )
        ple += [atom]

      self.particles += [ple]

  def __repr__(self):
    res = "PARTICLE FILTER with " + str(len(self.particles)) + " particles (hypotheses):\n"
    for i, ple in enumerate(self.particles):
      res += str(self.evaluateParticle(i)) + ", " + str(ple) + "\n"
    res += "\n----------------------"
    return res

  def evaluateParticle(self, i):
    res = 1.0
    if i < len(self.particles):
      for atom in self.particles[i]:
        res = min(res, atom.value)
    return res

  def evaluateHypothesis(self, particle):
    res = 1.0
    for atom in particle:
      res = min(res, atom.value)
    return res

  def pruneParticles(self):
    newPleList = []
    if self.numParticles != len(self.particles):
      ERROR("NUM PARTICLES NOT THE SAME AS PARTICLE LENGTH")

    for i in range(self.numParticles):
      if self.evaluateParticle(i) > self.stayThreshold:
        newPleList += [self.particles[i]]
    self.particles = newPleList
    INFO( "PRUNED %i PARTICLES" %(self.numParticles-len(self.particles)) )
    # need to add more particles

  def lowVarianceSampling(self):
    uniqueParticles = self.getUniqueParticleSet()
    self.particles = []
    
    samplingRegions = np.zeros( (len(uniqueParticles) ) )
    for i in range( len(uniqueParticles) ):
      if i == 0:
        samplingRegions[0] = self.evaluateHypothesis( uniqueParticles[0] ) - 0.5
      else:
        samplingRegions[i] = samplingRegions[i-1] + self.evaluateHypothesis( uniqueParticles[i] ) - 0.5
    # normalize
    samplingRegions /= samplingRegions[ len(samplingRegions)-1 ]

    # sample over a circle
    sampledPoints = np.arange(0,1, 1/self.numParticles) + 0.001
    for sample in sampledPoints:
      i = 0
      # find the region it's in
      while i < len(samplingRegions) and sample > samplingRegions[i]:
        i += 1
      # add the particle
      self.particles += [ copy.deepcopy(uniqueParticles[i]) ]
  
  def sampleSomeParticles(self):
    uniqueParticles = self.getUniqueParticleSet()
    samplingRegions = np.zeros( (1,len(uniqueParticles)) )
    for i in range( len(uniqueParticles) ):
      if i == 0:
        samplingRegions[0] = self.evaluateHypothesis( uniqueParticles[0] ) - 0.5
      else:
        samplingRegions[i] = samplingRegions[i-1] + self.evaluateHypothesis( uniqueParticles[i] ) - 0.5
    # normalize
    samplingRegions /= samplingRegions[ len(samplingRegions)-1 ]

    # sample uniformly 
    numPointsToSample = self.numParticles - len(self.particles)
    sampledPoints = np.random.uniform( 0,1,numPointsToSample )
    for sample in sampledPoints:
      i = 0
      # find the region it's in
      while i < len(samplingRegions) and sample > samplingRegions[i]:
        i += 1
      # add the particle
      self.particles += [ copy.deepcopy(uniqueParticles[i]) ]
      
  def getUniqueParticleSet(self):
    # TO DO: does that have to be unique or not???
    return copy.deepcopy(self.particles)









class Action:
  def __init__(self, name, params, variables, precondition, preconditionThreshold, deletes, addFunction ):
    self.name = name
    self.params = params # list of parameter terms
    self.vars = variables # list of variable terms used in the preconditions / effects
    self.precond = precondition # list of atoms, non-ground: only variables and parameters
    self.precondThreshold = preconditionThreshold # a float from 0 to 1
    self.effectDeletes = deletes # a list of atoms, to be compmletely removed
    self.effectAddFunction = addFunction  # a function that takes as an input the a list of parameters and the hypothesis, returns a list of atoms

  def evaluate(self, hyp, paramInstance):
    # hypothesis - a list of ground atoms
    # paramInstance - a dictionary of params to objects
    mapping = copy.deepcopy(paramInstance)
    # i am evaluating existentials
    # lets get all objects 
    objectList = []
    for atom in hyp:
      for term in atom.terms:
        notInObjectList = True
        for listTerm in objectList:
          if listTerm.sameAs(term):
            notInObjectList = False
            break
        if notInObjectList:
          objectList += [term]

    if len(objectList) == 0:
      ERROR("NO OBJECTS AT ALL MATE")
      return (False, hyp)

    inds = [0] * len(self.vars)
    i = len(self.vars)-1
    res = []
    while (True):
      objList = [ objectList[j] for j in inds ]
      if self.checkIfTypesWork( objList ) and self.checkIfPrecondsWork( objList, paramInstance, hyp ):
        res += [objList]
      inds[i] += 1
      while inds[i] == len(objectList):
        inds[i] = 0
        i -= 1
        if i < 0:
          break
        inds[i] += 1
      if i < 0:
        break

    if len(res) > 1:
      ERROR("MORE THAN 1 SOLUTION TO ASSIGNING EXISTENTIALS, ACTION INVALID")
      return (False, None)
    if len(res) == 0:
      WARN("NO SOLUTION TO ASSIGNING EXISTENTIALS, ACTION DOESN'T APPLY, OR ASSIGNMENT EXISTS BUT VALUE BELOW THRESHOLD")
      return (False, hyp)
    
    
    varAssignment = res[0]
    for i in range(len(varAssignment)):
      mapping[self.vars[i].value] = varAssignment[i].value

    # apply deletes:
    groundedDeletes = copy.deepcopy(self.effectDeletes)
    for atom in groundedDeletes:
      for term in atom.terms:
        term.makeObject( mapping[term.value] )
    # deletes are now grounded


    # apply deletes:
    resHyp = []
    for atom in hyp:
      notInDeletes = True
      for delAtom in groundedDeletes:
        if delAtom.sameAs(atom):
          notInDeletes = False
          break
      if notInDeletes:
         resHyp += [atom]

    # apply additions, which is already a list
    resHyp += self.effectAddFunction(mapping, hyp)

    return (True, resHyp)


  def checkIfTypesWork(self, objs):
    if len(objs) != len(self.vars):
      return False
    for i in range(len(objs)):
      if objs[i].type != self.vars[i].type:
        return False
    return True

  def checkIfPrecondsWork( self, varAssignment, paramInstance, hyp):
    # generate new precondition list with atoms
    groundedPreconds = copy.deepcopy(self.precond)
    mapping = dict()
    for key in paramInstance:
      mapping[key] = paramInstance[key]
    for i in range(len(self.vars)):
      mapping[self.vars[i].value] = varAssignment[i].value

    # ground all atoms in the precondition
    for atom in groundedPreconds:
      for term in atom.terms:
        term.makeObject( mapping[term.value] )

    for atom in groundedPreconds:
      existsSame = False
      for hypAtom in hyp:
        if hypAtom.sameAs(atom):
          existsSame = True
          atom.value = hypAtom.value
          break
      if not existsSame:
        return False
    
    # great, all same exist
    # finally, evaluate the value
    res = 1.0
    for atom in groundedPreconds:
      res = min(res, atom.value)

    if res >= self.precondThreshold:
      return True
    else:
      return False

class Predicate:
  def __init__(self, name, arity, types):
    self.name = name # armEmpty, pos, movable, etc
    self.arity = arity # predicate arity
    self.types = types # type of every term to the predicate
    if len(self.types) != self.arity:
      ERROR("NUMBER OF TYPES DOESN'T MATCH PREDICATE ARITY")
      print(name, arity, types)
      ERROR("NUMBER OF TYPS DOESN'T MATCH PREDICATE ARITY")

  def __repr__(self):
    res = self.name + "("
    for obj_type in self.types:
      res += " " + obj_type.__repr__()
    res += " )"
    return res

  def sameAs(self, op):
    if self.name != op.name or self.arity != op.arity:
      return False
    for i in range(self.arity):
      if self.types[i] != op.types[i]:
        return False
    return True

class Atom:
  def __init__(self, predicate, terms, value=None):
    self.predicate = predicate
    self.terms = terms
    if type(terms) != list:
      ERROR("TERMS MUST BE A LIST")
      print(predicate, terms, value)
      ERROR("TERMS MUST BE A LIST")

    if len(terms) != self.predicate.arity:
      ERROR("NUMBER OF TERMS DOESN'T MATCH PREDICATE ARITY")
      print(predicate, terms, value)
      ERROR("NUMBER OF TERMS DOESN'T MATCH PREDICATE ARITY")

    allObjects = True
    for term in terms:
      if not term.isObject():
        allObjects = False
    
    if allObjects and value == None and self.predicate.arity != 0:
      ERROR("MUST PROVIDE VALUE WHEN INITING A GROUND ATOM")
      print(predicate, terms, value)
      ERROR("MUST PROVIDE VALUE WHEN INITING A GROUND ATOM")

    elif not allObjects and value != None and self.predicate.arity != 0:
      ERROR("CAN'T PROVIDE VALUE WHEN INITING A NON-GROUND ATOM")
      print(predicate, terms, value)
      ERROR("CAN'T PROVIDE VALUE WHEN INITING A NON-GROUND ATOM")

    if value != None:
      assert(0 <= value <= 1)
    
    self.value = value # between 0 and 1 unless not none

  def isGround(self):
    # atom is ground, i.e., all of its terms are objects
    for term in self.terms:
      if term.isVariable() or term.isParam():
        return False
    return True

  def __repr__(self):
    res = ""
    if self.value != None:
      res += str(self.value) + " = " 
    res += self.predicate.name + "("
    for term in self.terms:
      res += " " + term.__repr__()
    res += " )"
    return res

  def sameAs(self, op):
    if not self.isCompatibleWith(op):
      return False
    
    for i in range( self.predicate.arity ):
      if not self.terms[i].sameAs( op.terms[i] ):
        return False
    return True

  def isCompatibleWith( self, op ):
    # same predicate
    if self.predicate.sameAs(op.predicate):
      return True
    return False

  def isMoreGeneralThan( self, op ):
    if not self.isCompatibleWith( op ):
      ERROR( "ATTEMPTING TO CHECK GENERALITY WITHOUT CHECKING COMPATIBILITY %s %s" %(self.__repr__(), op.__repr__()) )
      return
    for i in range(self.predicate.arity):
      term1 = self.terms[i]
      term2 = op.terms[i]
      # TO DO: literal generality is sketchy for variables; a variable is more general than a param, sure; 
      # but what about variable vs variable?
      # i guess i'd only call this function to compare variable to param / non-params
      if not term1.isMoreGeneralThan(term2):
        return False
    return True

class Term:
  def __init__(self, valueType, termType, value):
    self.valueType = valueType # constants, parameters, variables. constants are more specific than parameters, more specific than variables
    self.type = termType # i am dealing with typed objects: obj, pos, conf
    self.value = value

  def get(self):
    return self.value

  def sameAs(self, term):
    if self.type == term.type and self.valueType == term.valueType and self.value == term.value:
      return True
    return False
      
  def makeVariable(self, varName):
    self.value = varName
    self.valueType = "variable"

  def makeParam(self, paramName):
    self.value = paramName
    self.valueType = "param"

  def makeObject(self, value):
    self.value = value
    self.valueType = "object"

  def isObject(self):
    return self.valueType == "object"

  def isVariable(self):
    return self.valueType == "variable"

  def isParam(self):
    return self.valueType == "param"

  def __repr__(self):
    return str(self.value)
    return "(" + self.type + "," + str(self.value) + ")"

  def isMoreGeneralThan( self, term ):
    if self.type != term.type:
      ERROR( "COMPARING GENRALITY FOR TERMS OF DIFFERENT TYPES: %s %s" %( self.__repr__(), term.__repr__() ) )
      return

    st = self.valueType
    ot = term.valueType
    if st == "variable":
      if ot != "variable":
        return True
      WARN("COMPARING GENERALITY OF VARIABLES %s %s" %( self.__repr__(), term.__repr__() ))
      if ot == "variable" and self.value == term.value:
        return True
    elif st == "param" or st == "object":
      if st == ot and self.value == term.value:
        return True
    return False


obstacle1 = [ (50,0), (300, 50) ]
obstacle2 = [ (50,150), (100, 400) ]
obstacle3 = [ (350,200), (400, 350) ]
obstacles = [obstacle1, obstacle2, obstacle3]


class Tester:
  def __init__(self, trueHyp, actionList, predList, filterInitialization, numParticles = 20, stayThreshold = 0.5 ):
    self.trueHypothesis = trueHypothesis
    self.particleFilter = ParticleFilter( predList, filterInitialization, numParticles, stayThreshold )
    self.actions = dict()
    for action in actionList:
      self.actions[action.name] = action
    self.predicates = dict()
    for predicate in predList:
      self.predicates[predicate.name] = predicate


    tk = Tk()
    tk.withdraw()
    top = Toplevel(tk)
    top.wm_title('Grid')
    top.protocol('WM_DELETE_WINDOW', top.destroy)
    self.width = 500
    self.height = 500
    self.robot_rad = 5
    self.canvas = Canvas(top, width=self.width, height=self.height, background=BACKGROUND)
    self.canvas.pack()
    self.cells = {}
    self.environment = []
    self.applyAction( "physics", {} )
    self.drawRobots()
    
  def applyAction( self, actionName, paramInstance ):
    if actionName in ( "obsPos" ):
      ERROR("CALLING APPLY ACTION ON ACTION THAT DON'T GET APPLIED")
      return 
    
    if actionName == "move":
      # apply on the true hypothesis first
      (res, newHyp) = self.actions["perfect_move"].evaluate( self.trueHypothesis, paramInstance )
    else:
      (res, newHyp) = self.actions[actionName].evaluate( self.trueHypothesis, paramInstance )
    
    if res:
      self.trueHypothesis = newHyp
    else:
      ERROR("COULDN'T APPLY ACTION TO TRUE HYPOTHESIS")
    
    for i in range( len(self.particleFilter.particles) ):
      (res, newHyp) = self.actions[actionName].evaluate( self.particleFilter.particles[i], paramInstance )
      if res:
        self.particleFilter.particles[i] = newHyp
      else:
        ERROR("COULDN'T PERFORM ACTION FOR HYP %s"%(str(newHyp)))

  def observe(self):
    truePos = None
    for atom in self.trueHypothesis:
      if atom.predicate.sameAs( self.predicates["atPos"] ):
        truePos = atom.terms[0].value

    if truePos == None:
      ERROR("COULDN'T GET TRUE POSITION AT OBSERVE IN TESTER")
    
    paramInstance = { "TruePos": truePos }

    for i in range( len(self.particleFilter.particles) ):
      (res, newHyp) = self.actions["obsPos"].evaluate( self.particleFilter.particles[i], paramInstance )
      if res:
        self.particleFilter.particles[i] = newHyp
      else:
        ERROR("COULDN'T OBSERVE FOR HYP %s"%(str(newHyp)))
    
  def __repr__(self):
    res = "TRUE HYPOTHESIS:\n"
    res += str(self.trueHypothesis) + "\n-------------------------------------\n"
    res += "PARTICLES:\n"
    for ple in self.particleFilter.particles:
      res += str(ple) + "\n"
    res += "-------------------------------------\n"
    return res

  def getPos(self, hyp):
    for atom in hyp:
      if atom.predicate.sameAs( self.predicates["atPos"] ):
        return atom.terms[0].value
    ERROR("NO POS IN HYP")
    return None


  def drawRobots(self, diffSize = False):
    self.canvas.delete('all')
    scalingFactor = 15

    self.environment += [ 
          self.canvas.create_rectangle(-100, -100, self.width/2, self.height+100,
                                      fill=MOVABLE_COLORS[0], outline=MOVABLE_COLORS[0], width=0) ]
    self.environment += [ 
          self.canvas.create_rectangle(self.width/2, -100, self.width+100, self.height+100,
                                      fill=MOVABLE_COLORS[1], outline=MOVABLE_COLORS[1], width=0) ]

    for obstacle in obstacles:
      self.environment += [ 
          self.canvas.create_rectangle(obstacle[0][0], obstacle[0][1], obstacle[1][0], obstacle[1][1],
                                      fill=IMMOVABLE_COLORS[0], outline=IMMOVABLE_COLORS[0], width=0) ]
    

    for ple in self.particleFilter.particles:
      (x,y) = self.getPos(ple)
      if diffSize:
        pleSize = self.robot_rad * ( self.particleFilter.evaluateHypothesis( ple ) )
      else:
        pleSize = self.robot_rad * 0.5
      x = self.width/2 + x * scalingFactor
      y = self.height/2 + y * scalingFactor
      self.environment.append(self.canvas.create_oval(x - pleSize, y - pleSize, x + pleSize, y + pleSize, fill='black', width=0))

    (x,y) = self.getPos(self.trueHypothesis)
    pleSize = 1.0 * self.robot_rad
    pleSize = 3
    x = self.width/2 + x * scalingFactor
    y = self.height/2 + y * scalingFactor
    self.environment.append(self.canvas.create_oval(x - pleSize, y - pleSize, x + pleSize, y + pleSize, fill='red', width=0))


  def testMode(self):
    while(True):
      cmd = user_input("CMD: ")
      cmd_valid = True
      if cmd in ('w'):
        self.applyAction( "move", { "Disp": (0, -1) } )
        self.applyAction( "physics", {} )
        self.drawRobots(False)
      elif cmd in ('a'):
        self.applyAction( "move", { "Disp": (-1, 0) } )
        self.applyAction( "physics", {} )
        self.drawRobots(False)
      elif cmd in ('s'):
        self.applyAction( "move", { "Disp": (0, 1) } )
        self.applyAction( "physics", {} )
        self.drawRobots(False)
      elif cmd in ('d'):
        self.applyAction( "move", { "Disp": (1, 0) } )
        self.applyAction( "physics", {} )
        self.drawRobots(False)
      elif cmd in ('o'):
        self.observe()
        self.applyAction( "physics", {} )
        self.particleFilter.pruneParticles()
        self.particleFilter.lowVarianceSampling()
        self.drawRobots(True)
      elif cmd in ('n', 'p'):
        self.particleFilter.pruneParticles()
        self.particleFilter.lowVarianceSampling()
        self.drawRobots(True)
      elif cmd == "done":
        print("Exitting")
        sys.exit()
      else:
        print("\tCommand invalid")
        cmd_valid = False
      # if cmd_valid:
        # self.drawRobots()


# --------------------------------------------------------------------------------------------------------------------------


atPos = Predicate("atPos", 1, ["2-dim"])
physicsOk = Predicate( "physicsOk", 0, [] )

# --------------------------------------------------------------------------------------------------------------------------

# move action:
# parameter: DISP - 2-dim
# precond:
  # atPos(X) AND physicsOk > 0.5?
# deletes:
  # physicsOk, atPos(X)
# adds:
  # eval(ple, DISP) - function will output a list of atoms; doesn't change certainty (?)

Disp = Term( "param", "2-dim", "Disp" )
X = Term( "variable", "2-dim", "X" )
move_params = [ Disp ]
move_vars = [ X ]
move_precond = [ Atom( atPos, [X] ), Atom( physicsOk, [] ) ]
move_precond_thresh = 0.5
move_effect_deletes = [ Atom( atPos, [X] ), Atom( physicsOk, [] ) ]

MOVEMENT_UNCERTAINTY = 1.5

def move_add_function(varParamMapping, hyp):
  lastPos = np.array(varParamMapping["X"])
  delta = np.array(varParamMapping["Disp"])
  value = -1.0
  for atom in hyp:
    if atom.predicate.sameAs(atPos) and atom.terms[0].value == varParamMapping["X"]:
      value = atom.value
  if value < 0:
    ERROR("COULDN'T FIND THE RIGHT ATOM")
  x_delta = np.array([1,0]) * np.random.uniform(-MOVEMENT_UNCERTAINTY, MOVEMENT_UNCERTAINTY, 1)
  y_delta = np.array([0,1]) * np.random.uniform(-MOVEMENT_UNCERTAINTY, MOVEMENT_UNCERTAINTY, 1)
  newPos = delta + lastPos + x_delta + y_delta
  return [Atom( atPos, [ Term( "object", "2-dim", tuple(newPos) ) ], value )]

def perfect_move_add_function(varParamMapping, hyp):
  lastPos = np.array(varParamMapping["X"])
  delta = np.array(varParamMapping["Disp"])
  value = -1.0
  for atom in hyp:
    if atom.predicate.sameAs(atPos) and atom.terms[0].value == varParamMapping["X"]:
      value = atom.value
  if value < 0:
    ERROR("COULDN'T FIND THE RIGHT ATOM")
  newPos = delta + lastPos

  x = newPos[0]*15 + 250
  y = newPos[1]*15 + 250
  # don't into an obstacle
  for obstacle in obstacles:
    if obstacle[0][0] <= x <= obstacle[1][0] and obstacle[0][1] <= y <= obstacle[1][1]:
      newPos = lastPos
  return [Atom( atPos, [ Term( "object", "2-dim", tuple(newPos) ) ], value )]

move_action = Action( "move", move_params, move_vars, move_precond, move_precond_thresh, move_effect_deletes, move_add_function )

perfect_move_action = Action( "perfect_move", move_params, move_vars, move_precond, move_precond_thresh, move_effect_deletes, perfect_move_add_function )

# --------------------------------------------------------------------------------------------------------------------------
# observePos action:
# parameter: X - 2-dim 
# precond:
  # atPos(X) >= 0.0
# deletes:
  # atPos(X)
# adds:
  # eval(Xple, X) - function will output a list of atoms;
  # will output atPos(X) atom with some measured certainty

X = Term( "variable", "2-dim", "X" )
TruePos = Term( "variable", "2-dim", "Pos" )
obsPos_params = [ TruePos ]
obsPos_vars = [ X ]
obsPos_precond = [ Atom( atPos, [X] ) ]
obsPos_precond_thresh = 0.0
obsPos_effect_deletes = [ Atom( atPos, [X] ) ]

UNKNOWN_DISTANCE = 2.0

def obsPos_add_function(varParamMapping, hyp):
  pos = np.array(varParamMapping["X"])
  truePos = np.array(varParamMapping["TruePos"])
  # distance:
  dist = np.linalg.norm( np.array(pos)-np.array(truePos) )
  if pos[0] < 0:
    return [ Atom( atPos, [Term( "object", "2-dim", tuple(pos) )],  0.51  ) ]
  return [ Atom( atPos, [Term( "object", "2-dim", tuple(pos) )],  max( (1 - dist/(2 * UNKNOWN_DISTANCE) ), 0.0)  ) ]

obsPos_action = Action( "obsPos", obsPos_params, obsPos_vars, obsPos_precond, obsPos_precond_thresh, obsPos_effect_deletes, obsPos_add_function )

# --------------------------------------------------------------------------------------------------------------------------

# physics checker:
# parameter: nothing
# precond: atPos(X) >= 0.0, should be >= 0.5
# delete:
  # physicsOk
# adds:
  # eval(ple)

X = Term( "variable", "2-dim", "X" )
physics_params = []
physics_vars = [ X ]
physics_precond = [ Atom( atPos, [X] ) ] # car is at some position
physics_precond_thresh = 0.0
physics_effect_deletes = [ Atom( physicsOk, [] ) ]

def physics_add_function(varParamMapping, hyp):
  pos = None
  for atom in hyp:
      if atom.predicate.sameAs( atPos ):
        pos = atom.terms[0].value
  if pos == None:
    ERROR("NO POS IN HYP")
  
  x = pos[0]*15 + 250
  y = pos[1]*15 + 250
  for obstacle in obstacles:
    if obstacle[0][0] <= x <= obstacle[1][0] and obstacle[0][1] <= y <= obstacle[1][1]:
      return [ Atom( physicsOk, [], 0.0 ) ]

  return [ Atom( physicsOk, [], 1.0 ) ]

physics_action = Action( "physics", physics_params, physics_vars, physics_precond, physics_precond_thresh, physics_effect_deletes, physics_add_function )

# --------------------------------------------------------------------------------------------------------------------------

# collision checks

# introduce a set of obstacles

# if the true point is an obstacle - throw an error

# if a generic point is in an obstacle - 0 or 1

# may be how close it is to the obstacle too? no, just inside the obstacle cause that's not that hard


# obstacle2 = [ (0,0), (100, 300) ]




# --------------------------------------------------------------------------------------------------------------------------


trueHypothesis = [ Atom( atPos, [Term("object", "2-dim", (0,0) )], 1.0 ), Atom( physicsOk, [], 1.0 ) ] 
actionList = [ move_action, perfect_move_action, obsPos_action, physics_action ]
predList = [ atPos, physicsOk ]
filterInitialization = initPreds
numParticles = 40
stayThreshold = 0.5
test = Tester( trueHypothesis, actionList, predList, filterInitialization, numParticles, stayThreshold )





